<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/data', ['as' => 'home.data', 'uses' => 'HomeController@data']);
Route::get('/show', ['as' => 'home.show', 'uses' => 'HomeController@show']);

Route::get('/test', ['as' => 'test', 'uses' => 'TestController@test']);