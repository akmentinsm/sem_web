const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');
mix.js([
    'resources/js/app.js',
    // 'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    ], 'public/js')
    .sass('resources/sass/app.scss', 'public/css/sass.css')
    .combine([
        'public/css/sass.css',
        'public/css/styles.css'
    ],
        'public/css/app.css')
    .version();

// mix.sass('resources/assets/sass/app.scss', 'public/css')