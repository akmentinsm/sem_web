<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EasyRdf_Sparql_Client;
use BorderCloud\SPARQL\SparqlClient;
use App\SPARQLQueryDispatcher;
use DarkSky;
// use EasyRdf_Sparql_Client;
// use easyrdf\easyrdf\lib\EasyRdf\Sparql\Client;
// use BorderCloud\SPARQL\SparqlClient;

class TestController extends Controller
{

    function test()
    {
        $sparql = new EasyRdf_Sparql_Client('http://localhost:5820/sigulda_database/query');

        // $result = $s2->query(
        //     '
        //     SELECT *
        //     WHERE {
        //         ?uri "_:bnode_481a47de_b1f1_4c25_a1c5_9301fabb1b40_254" ?p ?o.
        //         FILTER (?uri = <_:bnode_481a47de_b1f1_4c25_a1c5_9301fabb1b40_254>s) 
        //     }
        //     '
        // );

        $res = $sparql->query(
            '
            SELECT distinct ?eventStartDateLabel ?weatherDateLabel ?eventLabel ?eventTimeLabel ?temperatureLabel
            WHERE {
                ?eventStartDate <http://localhost/data.csv#DateStart> ?eventStartDateLabel .
                ?weatherDate <http://localhost/weather.csv#WeatherDate> ?weatherDateLabel .
                
                filter (?eventStartDateLabel = ?weatherDateLabel)
            }
            '
        );

        dd($res);

        $subjects = $sparql->query(
            'SELECT * {
                SELECT ?s WHERE { ?s ?p ?o }
            }'
        );
        // $results = [];
        foreach ($subjects as $subject) {
            $results = $sparql->query(
            'SELECT ?o WHERE { ?s ?p ?o }'
        );
        }
        dd($results);
        foreach ($result as $row) {
            dd($row);
            echo "<li>" . ($row->o) . "</li>\n";
        }
    }
}
