<?php

namespace App\Http\Controllers;

use EasyRdf_Sparql_Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DateTime;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $sparql = new EasyRdf_Sparql_Client('http://localhost:5820/sigulda_database/query');

        if(isset($request->type)){
            switch($request->type){
                case '1':
                    $result = $sparql->query(
                        '
                        SELECT ?Event ?DateString ?Date
                        WHERE {
                            ?x <http://localhost/data.csv#Event> ?Event .
                            ?x <http://localhost/data.csv#Date> ?DateString .
                            ?x <http://localhost/data.csv#DateStart> ?Date .
                            FILTER ( ?Date = "'.$request->date_start.'"^^xsd:date )
                        } ORDER BY ASC(?Date)
                        '
                    );
                    return view('home', ['result' => $result]);
                case '2':
                    $result = $sparql->query(
                        '
                        SELECT ?Event ?DateString ?Date
                        WHERE {
                            ?x <http://localhost/data.csv#Event> ?Event .
                            ?x <http://localhost/data.csv#Date> ?DateString .
                            ?x <http://localhost/data.csv#DateStart> ?Date .
                            ?x <http://localhost/data.csv#DateEnd> ?DateEnd .
                            FILTER (  (?Date <= "'.$request->date_start.'"^^xsd:date && ?DateEnd >= "'.$request->date_start.'"^^xsd:date) || (?Date = "'.$request->date_start.'"^^xsd:date && ?DateEnd = "-"))
                        } ORDER BY ASC(?Date)
                        '
                    );
                    return view('home', ['result' => $result]);
                default:
                    $result = $sparql->query(
                        "
                        SELECT ?Event ?DateString ?Date
                        WHERE {
                            ?x <http://localhost/data.csv#Event> ?Event .
                            ?x <http://localhost/data.csv#Date> ?DateString .
                            ?x <http://localhost/data.csv#DateStart> ?Date .
                        } ORDER BY ASC(?Date)
                        "
                    );
                    return view('home', ['result' => $result]);
            }
        }

        $result = $sparql->query(
            "
            SELECT ?Event ?DateString ?Date
            WHERE {
                ?x <http://localhost/data.csv#Event> ?Event .
                ?x <http://localhost/data.csv#Date> ?DateString .
                ?x <http://localhost/data.csv#DateStart> ?Date .
            } 
            "
        );

        return view('home', ['result' => $result]);
    }

    public function show(Request $request)
    {
        $sparql = new EasyRdf_Sparql_Client('http://localhost:5820/sigulda_database/query');

        // get event data
        $result = $sparql->query(
            '
            SELECT ?Event ?DateString ?DateStart ?DateEnd ?Time
            WHERE {
                ?x <http://localhost/data.csv#Event> ?Event .
                ?x <http://localhost/data.csv#Date> ?DateString .
                ?x <http://localhost/data.csv#DateStart> ?DateStart .
                ?x <http://localhost/data.csv#DateEnd> ?DateEnd .
                ?x <http://localhost/data.csv#Time> ?Time .
                FILTER ( ?Event =  "'.$request->event.'")
            } 
            '
        );

        // get weather data
        $weatherResult = $sparql->query(
            '
            SELECT ?Temperature ?Date ?Time
            WHERE {
                ?x <http://localhost/weather.csv#WeatherDate> ?Date .
                ?x <http://localhost/weather.csv#WeatherTime> ?Time .
                ?x <http://localhost/weather.csv#Temperature> ?Temperature .
                FILTER ( ?Date = "'.$request->date.'"^^xsd:date)
            } 
            '
        );

        // if weather data not empty, get closest temperature to event time
        if(!empty($weatherResult[0])){
            foreach ($weatherResult as $row) {
                $interval[] = abs(strtotime((string) $result[0]->Time) - strtotime((string) $row->Time));
            }
            asort($interval);
            $closest = key($interval);

            $temperature = (string) $weatherResult[$closest]->Temperature;
        }else
            $temperature = 'No data';

        $time = $result[0]->Time == '00:00:00' ? 'All day event' : (string) $result[0]->Time;

        // Pieņemot, ka esošajos datos katra pasākuma nosaukums ir unikāls,
        // sparql vairājums vienmēr atgriež tikai vienu trijnieku
        $data = [];
        $data['event'] = (string) $result[0]->Event;
        $data['temperature'] = $temperature;
        $data['date_string'] = (string) $result[0]->DateString;
        $data['start_date'] = (string) $result[0]->DateStart;
        $data['end_date'] = (string) $result[0]->DateEnd;
        $data['time'] = $time;

        return $data;
    }

}
