<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>sem web</title>

	<!-- Bootstrap core CSS -->
	<link href = {{ asset("css/app.css") }} rel="stylesheet" />

	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<div class="container">
		<br>
		<div class="row">
			<div class="box-body">
				{{ Form::open(['route' => 'home', 'method' => 'GET', 'data-prevent-submit' => 'true']) }}
					{{ Form::date('date_start', 'Select date') }}
					{{ Form::select('type', ['1' => 'Events starting in date', '2' => 'Events happening in date', '3' => 'All events'], '3') }}
					{{ Form::submit('Get events', ['class' => 'btn btn-primary']) }}
				{{ Form::close() }}
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<div class="list-group">
					@for ($i=0; $i<$result->count(); $i++)
						<button type="button" class="list-group-item list-group-item-action event" id="{{ $result[$i]->Date }}">Date: {{ $result[$i]->DateString }}<br><span id="{{ $result[$i]->Event }}">Event: {{ $result[$i]->Event }}</span></button>
					@endfor
				</div>
			</div>
			<div class="col-md-6">
    			<div class="card" style="display: none">
      				<div class="card-body">
        				<h5 class="card-title">Event: <span id="event"></span></h5>
        				<p class="card-text">Temperature: <span id="temperature"></span></p>
        				<p class="card-text">Date: <span id="date_string"></span></p>
        				<p class="card-text">Start date: <span id="start_date"></span></p>
        				<p class="card-text">End date: <span id="end_date"></span></p>
        				<p class="card-text">Time: <span id="time"></span></p>
        				<a href="#" class="btn btn-primary" id="close">Close</a>
      				</div>
    			</div>
  			</div>
  		</div>
  	</div>
</body>

<script src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script type="text/javascript">
	$('.event').click(function(){
		var date = $(this).attr('id');
		var event = $(this).find('span').attr('id');

		$.ajax
		({ 
			url: '/show',
			data: {"event": event, "date": date},
			type: 'GET',
			success: function(result)
			{
				$(window).scrollTop(0);
				$( ".card" ).show();
				$( "#event" ).html(result.event);
				$( "#temperature" ).html(result.temperature);
				$( "#date_string" ).html(result.date_string);
				$( "#start_date" ).html(result.start_date);
				$( "#end_date" ).html(result.end_date);
				$( "#time" ).html(result.time);
			}
		});
	});

	$('#close').click(function(){
		$( ".card" ).hide();
	});
</script>
</html>